function estaVacio(valor){
	if(valor==null||valor.length==0||valor==""){
		return true;
	}
	return false;
}
function esAlfanumerico(valor){
	var patron=/^[a-zA-Z0-9]{1,8}$/;
	if(patron.test(valor)){
		return true;
	}else{
		return false;
	}
}
function sonIguales(valor1,valor2){
	if(valor1==valor2){
		return true;
	}else{
		return false;
	}
}
function esCorrectoEmail(valor1){
	var patron=/\S+@\S+/;

	if(patron.test(valor1)){
		return true;
	}
	return false;
}
function esCorrectoTelefono(valor1){
	var patron=/^[+]{1}[0-9]{1,11}$/;
	if(patron.test(valor1)){
		return true;
	}
	return false;
}
function comprobarLength(valor1,max){
	if(valor1 <= max){
		return true;
	}
	return false;
}





function validacion(){
	$("#esconder").hide(1000);
	var escritura=document.getElementById("escritura2");
	var nombre=document.getElementById("fname");
	var apellidos=document.getElementById("lname");
	var email=document.getElementById("email");
	var telefono=document.getElementById("phone");
	var textArea=document.getElementById("message");
	var todoCorrecto=true;
	var mensaje="";
	//Nombre
	if(!estaVacio(nombre.value)){
		if(esAlfanumerico(nombre.value)){
			nombre.style.border="1px solid green";
		}else{
			todoCorrecto=false;
			nombre.style.border="1px solid red";
			mensaje+="El campo nombre no es alfanumerico o su longitud excede 8 caracteres"+"<br>";
		}
	}else{
		todoCorrecto=false;
		nombre.style.border="1px solid red";
		mensaje+="No puede estar vacio el campo nombre"+"<br>";
	}
	//Apellidos
	if(!estaVacio(apellidos.value)){
		if(esAlfanumerico(apellidos.value)){
			if(!sonIguales(nombre.value,apellidos.value) && estaVacio(nombre.value)==false){
				apellidos.style.border="1px solid green";
			}else{
				todoCorrecto=false;
				apellidos.style.border="1px solid red";
				nombre.style.border="1px solid red";
				mensaje+="El nombre no puede ser identico al apellido"+"<br>";
			}
			
		}else{
			todoCorrecto=false;
			apellidos.style.border="1px solid red";
			mensaje+="El campo apellidos no es alfanumerico o su longitud excede 8 caracteres"+"<br>";
		}
	}else{
		todoCorrecto=false;
		apellidos.style.border="1px solid red";
		mensaje+="No puede estar vacio el campo apellidos"+"<br>";
	}
	//Correo electronico
	if(estaVacio(email.value)){
		todoCorrecto=false;
		email.style.border="1px solid red";
		mensaje+="No puede estar vacio el campo email"+"<br>";
	}else{
		if(esCorrectoEmail(email.value)){
			email.style.border="1px solid green";
		}else{
			todoCorrecto=false;
			email.style.border="1px solid red";
			mensaje+="No tiene un formato correcto el email"+"<br>";
		}
	}
	//Telefono
	if(!estaVacio(telefono.value)){
		if(esCorrectoTelefono(telefono.value)){
			telefono.style.border="1px solid green";
		}else{
			todoCorrecto=false;
			telefono.style.border="1px solid red";
			mensaje+="No tiene un formato correcto el telefono +34600900300 (ejemplo)"+"<br>";
		}
	}else{
		todoCorrecto=false;
		telefono.style.border="1px solid red";
		mensaje+="No puede estar vacio"+"<br>";
	}
	//Textarea
	if(!estaVacio(textArea.value)){
		if(comprobarLength(textArea.value.length,500)){
			textArea.style.border="1px solid green";
		}else{
			todoCorrecto=false;
			textArea.style.border="1px solid red";
			mensaje+="El textarea es demasiado largo, maximo son 500 caracteres!"+"<br>";
		}
	}else{
		todoCorrecto=false;
			textArea.style.border="1px solid red";
			mensaje+="No puede estar vacio el textarea"+"<br>";
	}
	
	if(todoCorrecto){
		this.form.submit();
	}else{	
		escritura.innerHTML=mensaje;
	}

}

document.getElementById("btnContact").addEventListener("click",validacion);
document.getElementById("message").addEventListener("keypress",function(){
	var escritura=document.getElementById("escrituraLength");
	escritura.innerHTML=this.value.length;
})
document.getElementById("message").addEventListener("onchange",function(){
	var escritura=document.getElementById("escrituraLength");
	escritura.innerHTML=this.value.length;
})